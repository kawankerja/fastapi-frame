# Kawan Kerja Fast Api Frame
## Installation

### Requirements

- mysql database
- python 3.7 or above

### Clone repository


    $ git clone url

### Make environment variable
- open terminal / cmd
- type

```
    $ python -m venv env
```

### Use environment variable
- open terminal / cmd
- type

```
    $ env\Scripts\activate
```
### Install dependency
- on selected environment variable in terminal / cmd
- type
```
    $ python.exe -m pip install --upgrade pip
    $ pip install -r .\requirements.txt
```

### Configure env file
- go to root folder
- rename .env_example file into .env
- change the following lines

```sh
HOST=localhost
DATABASE=fastapi
PORT=3306
USER=root
PASSWORD= 
JDBC=mysql
```

- adjust with your local mysql DB configuration

### Run Application

- go to root folder
- open terminal / cmd 
- type 


```
    $ python main.py
```






