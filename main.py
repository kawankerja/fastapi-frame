import uvicorn
from decouple import config

if __name__ == "__main__":
    #test env
    # print(config('USER'))
    uvicorn.run("app.api:app", host=config('SERVER_HOST'), port=config('SERVER_PORT'), reload=True)