from decouple import config

HOST = config('HOST',default='localhost')
DATABASE = config('DATABASE',default='fastapi')
PORT = config('PORT',default=3306,cast=int)
USER = config('USER',default='root')
PASSWORD = config('PASSWORD',default='')
JDBC = config('JDBC',default='mysql')