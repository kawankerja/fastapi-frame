from fastapi import FastAPI
from tortoise.contrib.fastapi import HTTPNotFoundError, register_tortoise
from .wrapper import models, add_router
from .settings import *

TORTOISE_ORM = {
    "connections": {"default": f'{JDBC}://{USER}:{PASSWORD}@{HOST}:{PORT}/{DATABASE}'},
    "apps": {
        "models": {
            "models": models,
            "default_connection": "default",
        },
    },
}


app = FastAPI()

register_tortoise(
    app, 
    config=TORTOISE_ORM,
    generate_schemas=True,
    add_exception_handlers=True
)

app = add_router(app)

@app.get("/")
async def root():
    return {"message": "Hello Bigger Applications!"}