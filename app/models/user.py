from tortoise import fields
from tortoise.models import Model
from tortoise.contrib.pydantic import pydantic_model_creator

class User(Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=250)
    email = fields.CharField(max_length=250)



UserSchema = pydantic_model_creator(User, name="User")
UserSchemaCreate = pydantic_model_creator(User, name="User", exclude_readonly=True)
