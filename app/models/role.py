from tortoise import fields
from tortoise.models import Model
from tortoise.contrib.pydantic import pydantic_model_creator

class Role(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=250)


RoleSchema = pydantic_model_creator(Role, name="Role")
RoleSchemaCreate = pydantic_model_creator(Role, name="Role", exclude_readonly=True)
