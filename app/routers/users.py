from fastapi_crudrouter.core.tortoise import TortoiseCRUDRouter
from ..models.user import *

router = TortoiseCRUDRouter(
    schema=UserSchema,
    create_schema=UserSchemaCreate,
    db_model=User,
    prefix="user"
)