from fastapi_crudrouter.core.tortoise import TortoiseCRUDRouter
from ..models.role import *

router = TortoiseCRUDRouter(
    schema=RoleSchema,
    create_schema=RoleSchemaCreate,
    db_model=Role,
    prefix="role"
)