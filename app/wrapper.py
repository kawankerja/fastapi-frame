from .routers import users , roles

models = []
models.append('app.models.user')
models.append('app.models.role')

def add_router(app):
    app.include_router(users.router)
    app.include_router(roles.router)
    return app